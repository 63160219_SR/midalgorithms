import java.util.Scanner;

public class A1_8 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int M = kb.nextInt();
		int MM;
		int[] Array;
		Array = new int[M];
		for (int i = 0; i < Array.length; i++) {
			int num = kb.nextInt();
			Array[i] = num;
			
		}
		for (int i = 0; i < Array.length/2; i++) {
			MM = Array[i]; //สร้าง ตัวแปล MM มาเก็บข้อมูล Array ในตำแหน่ง i สมติ i = 1 จะเป็น MM = 1
			Array[i] = Array[Array.length-1-i]; //ให้ Array ในตำแหน่ง 0 เท่ากับ Array ในตำแหน่งที่ 4
			Array[Array.length-1-i] = MM; // Array ตำแหน่งที่ 4 จะมีค่าเท่ากับ 1

		}
		for (int i = 0; i < Array.length; i++) {
			System.out.print(Array[i] + " ");
		}
		long startTime = System.nanoTime();
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		double seconds = (double) totalTime/1000000;
		System.out.println("\nTotalTime a programe "+seconds);
	}
}